Na realiza��o do presente projecto n�o foram estabelecidos diagramas com rela��es de intera��o (ou coopera��o) entre algoritmos ou tecnologias utilizados.

Tal deve-se ao facto de, ap�s a planifica��o e an�lise do sistema, a sua arquitectura ter indo a ser estabelecida � medida que ia crescendo quanto a processos e aplica��es funcionais, mantendo-se no entanto conscistente a rela��o entre o tratamento de dados entre as classes de linguagem de programa��o Java (e JSP) e o modo de armazenamento escolhido, a organiza��o de informa��o em ficheiros XML. 

A um processo global que inicialmente seria apenas realizado recorrendo a interfaces gr�ficas para a existencia da comunica��o entre estudantes e coordenadores foi adicionada a fun��o de reprodu��o de mensagens electr�nicas com hiperliga��es essenciais ao acesso do sistema, destinadas a realizar o controlo sobre os utilizadores passiveis de realizar e de vizualizar os conte�dos do sistema, neste caso estudantes e coordenadores.

O acesso ao sistema deve-se � exist�ncia de um servidor, o servidor escolhido foi o Tomcat, onde est�o alocadas as interfaces que estabelecem o portal da aplica��o do sistema para todos os utilizadores fornecerem os ficheiros XML com as informa��es requridas no estabelecimento dos m�todos de gest�o de dados.
